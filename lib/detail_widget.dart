import 'package:flutter/material.dart';
import 'package:flutter_sms/sms_model.dart';

class DetailWidget extends StatelessWidget {

  DetailWidget(this.sms);

  final Sms sms;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(sms?.message ?? 'Must specify the Sms for detail'),
    );
  }
}
