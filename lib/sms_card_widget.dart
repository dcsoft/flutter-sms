import 'package:flutter/material.dart';
import 'package:flutter_sms/detail_widget.dart';

import 'sms_model.dart';

class SmsCardWidget extends StatefulWidget {

  SmsCardWidget(this.sms, this.onSmsSelected);
  
  final Sms sms;
  final ValueChanged<Sms> onSmsSelected;

  @override
  // Each State implements a build method that associates the widget with the State
  State<SmsCardWidget> createState() => _SmsCardWidgetState();
}

class _SmsCardWidgetState extends State<SmsCardWidget> {

  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
            // Use inkwell for touch actions:  https://api.flutter.dev/flutter/material/Card-class.html
            splashColor: Colors.blue, //.withAlpha(30),
            onTap: () {
              widget.onSmsSelected(widget.sms);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.sms.name, style: TextStyle(fontWeight: FontWeight.bold)),
                Text(widget.sms.message),
              ],
            )));
  }
}
