import 'lax.dart';

class LaxMessages extends Lax {

    LaxMessages(String baseUrl, String apiKey) : super(baseUrl, apiKey);

    bool laxFailed(Map content) {
      int code = content['code'];
      return (code == null || code != 0);
    }

    String formatLaxError(string funcName, Map content) {
      
    }

    /*
    def laxFailed(self, content) -> bool:
        code = content.get('code')
        return code is None or code != 0

    def formatLaxError(self, func_name, content) -> str:
        return f'{func_name} failed with content:  {json.dumps(content, indent=2)}'

    def raise_failed(self, func_name, content):
        if self.laxFailed(content):
            raise LaxMessagesException(self.formatLaxError(func_name, content))


    */
}
