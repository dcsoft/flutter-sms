import "dart:convert";
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;

class Lax {
  Lax(this.baseUrl, this.apiKey);

  String baseUrl;
  String apiKey;
  String accessToken;

  Future<http.Response> rawSendLax(String url, [Map payload]) {
    payload = payload ?? Map<String, String>();
    payload['apiKey'] = apiKey;
    payload['apiVersion'] = '6';
    payload['device'] = 'nexus s';            // <-- this was specified in send_lax2
    if (accessToken != null)
      payload['accessToken'] = accessToken;   // <-- this was specified in send_lax3

    //String json = JsonEncoder.convert(payload);
    return http.post(url, body: payload);
  }

  Future<Map> sendLax(String url, [Map payload]) async {
    String laxUrl = path.join(baseUrl, url);
    http.Response response = await rawSendLax(laxUrl, payload);

    // Assume response is LAX formatted JSON with 'code' field of 0 if error
    return json.decode(response.body);
  }
}