import 'package:flutter/material.dart';

import 'sms_model.dart';
import 'sms_card_widget.dart';

class SmsListWidget extends StatelessWidget {

  SmsListWidget(this.smsMessages, this.onSmsSelected);

  // TODO:  remove copy of message list in the list widget
  final List<Sms> smsMessages;
  final ValueChanged<Sms> onSmsSelected;

  @override
  Widget build(BuildContext context) {
    /*
    return ListView.builder(
      itemCount: smsMessages.length,
      // A callback that returns a widget
      itemBuilder: (context, int) {
        // in our case, a message card 
        return SmsCardWidget(smsMessages[int]);
      },
    );
    */
    return ListView(
        children: smsMessages.map((sms) {
      return SmsCardWidget(sms, onSmsSelected);
    }).toList());
  }
}
