# flutter_sms

A new Flutter project.
Opens List View and Detail view on same screen as per:  https://iiro.dev/2018/01/28/implementing-adaptive-master-detail-layouts/
Click item in list to show in Detail view.
Keyboard in list doesn't work.

Demo video:  See https://dcsoft.com/private/line2/J2%20Flutter_SMS_Web.mp4


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
